GTDB Backend
====

This is the backend part for [AnnoTree](http://annotree.uwaterloo.ca).

Install
---
`pip install -r requirements.txt`

Connection to database is in `config.py`. Create `config.py`, using `sample-config.py` as an example, for security purpose please turn off read access for other group: `chmod o-r config.py`

**Development server and debug**

`python app.py` to start development server.
App is served on `localhost:5001`, you can change that in `app.run` line.

**Production**
It is recommended to create another user `gtdb_user` and change user and group to that user: `chown -R gtdb_user:gtdb_user <this repository>`

Then, to serve backend using apache WSGI module (assuming you have apache2 installed already):
```
sudo apt-get install libapache2-mod-wsgi
sudo a2enmod mod-wsgi
sudo service apache2 restart
```

change apache config file using the following as an example:

```
<VirtualHost example.com:80>
    WSGIDaemonProcess gtdb_user user=gtdb_user group=apache2 threads=5
    WSGIScriptAlias /gtdb-api /app/gtdb-backend/app.wsgi
    <Directory /app/gtdb-backend/>
        AllowOverride all
        WSGIProcessGroup gtdb_user
        WSGIApplicationGroup %{GLOBAL}
        Order deny,allow
        Allow from all
        Require all granted
    </Directory>
    ErrorLog ${APACHE_LOG_DIR}/gtdb_backend_error.log
    CustomLog ${APACHE_LOG_DIR}/gtdb_backend_access.log combined
</VirtualHost>
```

Request to `http://example.com/gtdb-api/gtdb_bacteria/tree` will be converted to `/gtdb_bacteria/tree` and sent to `app.wsgi`. You can modify API prefix by changing `WSGIScriptAlias` directive.

Issues and contributing
---
Please feel free to open an issue on Bitbucket page for developers to review.